#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This example demonstrates many of the 2D plotting capabilities
in pyqtgraph. All of the plots may be panned/scaled by dragging with 
the left/right mouse buttons. Right click on any plot to show a context menu.
"""

## Add path to library (just for examples; you do not need this)

import os
os.environ['PYQTGRAPH_QT_LIB'] = 'PyQt5'
import sys
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg
import pyqtgraph.opengl as gl



app = QtGui.QApplication(['a'])
L = 1000
w = gl.GLViewWidget()
w.opts['distance'] = 4*L
w.show()

N = 10000
dim = 3
pos =L*np.random.uniform(size=(N,dim))-L/2
color = np.concatenate((np.random.uniform(size=(N,dim)).T, [np.ones(N)])).T

#s1 = pg.ScatterPlotItem(pos=pos, size=15, color=(255, 0,0, .5))
s1 = gl.GLScatterPlotItem(pos=pos, size=5, color=color)
w.addItem(s1)

def update():
	global s1, pos, N, dim, L
	s1.setData(pos=pos)

timer = QtCore.QTimer()
timer.timeout.connect(update)
timer.start(50)

if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_() 
