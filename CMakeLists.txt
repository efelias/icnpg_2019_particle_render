cmake_minimum_required(VERSION 3.8)


set(CMAKE_CXX_STANDARD 11)

## ON corre en CPU, OFF corre en GPU con CUDA
if(NOT DEFINED IN_CPU)
    set(IN_CPU ON)
endif()

if(${IN_CPU})
    ## nombre del proyecto y lenguaje
    project(CUDA_Particles_Render LANGUAGES CXX)

    ## defines :
    set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DCPU")
    ## descomentar para distribucion esferica
    #set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DSPHERE -DWRONG")

    ## descomentar para distribucion esferica correcta
    #set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DSPHERE")

    ## descomentar para utilizacion de colormap hot random
    #set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DCMAP")

    ## descomentar para utilizacion de colormap hot uniforme
    #set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DCMAP -DUNIFORM")

    add_library(GLsystem SHARED
        glsystem.h glsystem.cpp
        posicioner.h posicioner.cpp
        render_particles.h render_particles.cpp
        shaders.h shaders.cpp
        )

    target_link_libraries(GLsystem PUBLIC "GL" "glut" "GLEW" "GLU" "m")

    add_executable(${PROJECT_NAME} "visualization.cpp")

else(${IN_CPU})## compilacion con nvcc, ejemplo extraido de la pagina de CUDA (CUDA+CMAKE)
    project(CUDA_Particles_Render LANGUAGES CXX CUDA)

    set(CUDA_INCLUDE "/usr/local/cuda/include")
    INCLUDE_DIRECTORIES(${CUDA_INCLUDE})
    link_directories("/usr/local/cuda/lib64/")

    ## defines :

    ## descomentar para distribucion esferica
    #set( CMAKE_CUDA_FLAGS "${CMAKE_CUDA_FLAGS} -DSPHERE -DWRONG")

    ## descomentar para distribucion esferica correcta
    #set( CMAKE_CUDA_FLAGS "${CMAKE_CUDA_FLAGS} -DSPHERE")

    ## descomentar para utilizacion de colormap hot random
    #set( CMAKE_CUDA_FLAGS "${CMAKE_CUDA_FLAGS} -DCMAP")

    ## descomentar para utilizacion de colormap hot uniforme
    #set( CMAKE_CUDA_FLAGS "${CMAKE_CUDA_FLAGS} -DCMAP -DUNIFORM")

    add_library(GLsystem SHARED
        glsystem.h glsystem.cu
        posicioner.h posicioner.cu
        render_particles.h render_particles.cpp
        shaders.h shaders.cpp
        )

    target_compile_features(GLsystem PUBLIC cxx_std_11)

    set_target_properties(GLsystem PROPERTIES
                    CUDA_SEPARABLE_COMPILATION ON)

    target_link_libraries(GLsystem PUBLIC "GL" "glut" "GLEW" "GLU" "m" "cudart")

    add_executable(${PROJECT_NAME} "visualization.cu")
endif(${IN_CPU})

target_link_libraries(${PROJECT_NAME} "GLsystem" )


